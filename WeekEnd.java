import java.util.List;
import java.util.ArrayList;

public class WeekEnd {
    private List<Personne> listeAmis;
    private List<Depense> listeDepenses;

    public WeekEnd() {
        this.listeAmis = new ArrayList<>();
        this.listeDepenses = new ArrayList<>();
    }

    public void ajouterPersonne(Personne personne) {this.listeAmis.add(personne);}
    public void ajouterPersonne(Personne... personnes) {
        for(Personne pers : personnes) {this.ajouterPersonne(pers);}
    }

    public void ajouterDepense(Depense dpense) {this.listeDepenses.add(dpense);}
    public void ajouterDepense(Depense... dpenses) {
        for(Depense dpse : dpenses) {this.ajouterDepense(dpse);}
    }

    // getters

    public List<Personne> getPersonnes() {return this.listeAmis;}
    public List<Depense> getDepenses() {return this.listeDepenses;}

    // totalDepensesPersonne prend paramètre une personne
    // et renvoie la somme des dépenses de cette personne.
    public int depenseParPersonne(Personne personne) {
        int depensePers = 0;
        for (Depense dpse : this.listeDepenses) {
            if (dpse.getPersonne() == personne)
                depensePers += dpse.getMontant();
        }
        return depensePers;
    }

    // totalDepenses renvoie la somme de toutes les dépenses.
    public int totalDepenses() {
        int total = 0;
        for (Depense dpse : this.listeDepenses)
            total += dpse.getMontant();
        return total;
    }

    // depenseMoyenne renvoie la moyenne des dépenses par
    // personne
    public double depenseMoyenne() {
        int dpseTot = this.totalDepenses();
        int nbPers = this.listeAmis.size();

        return dpseTot / nbPers;
    }

    // totalDepenseProduit prend un nom de produit en paramètre et renvoie la
    // somme des dépenses pour ce produit.
    // (du pain peut avoir été acheté plusieurs fois...)
    public int totalDepenseProduit(String nomProduit){
        int totalDpseProduit = 0;
        for (Depense dpse : this.listeDepenses) {
            if (dpse.getProduit() == nomProduit)
                totalDpseProduit += dpse.getMontant();
        }
        return totalDpseProduit;
    }

    // avoirParPersonne prend en paramètre une personne et renvoie
    // son avoir pour le week end.
    public double avoirPersonne(Personne personne){
        double moyenne = this.depenseMoyenne();
        int dpsePers = this.depenseParPersonne(personne);
        return moyenne - dpsePers;
    }

    public void afficheAvoirPersonnes() {
        for (Personne pers : this.listeAmis)
            System.out.println(pers.getNom() + " : " + this.avoirPersonne(pers));
    }
}