import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

public class TestWeekEnd {

   private Personne p1, p2, p3;
   private Depense d1, d2, d3;
   private WeekEnd we;


   @Before
   public void initialisation(){
      this.p1 = new Personne("p1");
      this.p2 = new Personne("p2");
      this.d1 = new Depense("d1", 10, p1);
      this.d2 = new Depense("d2", 15, p1);
      this.d3 = new Depense("d3", 5, p2);
      this.we = new WeekEnd();
      this.we.ajouterPersonne(p1, p2, p3);
      this.we.ajouterDepense(d1, d2, d3);

   }
   /** -------------------------------------------
   
       TEST : getters de Depense
   
    --------------------------------------------*/

   @Test
   public void testGetProduit() {
      assertEquals(d1.getProduit(), "d1");
   }

   @Test
   public void testGetMontant() {
      assertEquals(d1.getMontant(), 10);
   }

   @Test
   public void testGetPersonne() {
      assertEquals(d1.getPersonne(), p1);
   }

   /** -------------------------------------------
   
       TEST : getters de Personne
   
    --------------------------------------------*/

   @Test
   public void testGetNom() {
      assertEquals(p1.getNom(), "p1");
   }

   /** -------------------------------------------
    
      TEST : méthodes de WeekEnd
    
   ---------------------------------------------*/

    @Test
     public void testDepenseParPersonne() {
        assertEquals(we.depenseParPersonne(p1), 25);
     }

     @Test
     public void testTotalDepenses() {
        assertEquals(we.totalDepenses(), 30);
     }

     @Test
     public void testTotalDepenseProduit() {
      assertEquals(we.totalDepenseProduit("d1"), 10);
     }

     @Test
     public void testDepenseMoyenne() {
      assertEquals(we.depenseMoyenne(), 10.0, 0.1);
     }

     @Test
     public void testAvoirPersonne() {
      assertEquals(we.avoirPersonne(p1), -15.0, 0.1);
      assertEquals(we.avoirPersonne(p2), 5.0, 0.1);
     }

}
